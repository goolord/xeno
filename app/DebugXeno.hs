{-# LANGUAGE QuasiQuotes #-}
import qualified Data.ByteString as BS
import Text.InterpolatedString.Perl6 (qc)

import Xeno.SAX
import Xeno.Unicode


printProcessing :: (VectorizedString str) => str -> IO ()
printProcessing =
    process $ Process
        { openF    = \tag        -> putStrLn [qc|openF: "{tag}"|]
        , attrF    = \name value -> putStrLn [qc|attrF: "{name}" = "{value}"|]
        , endOpenF = \tag        -> putStrLn [qc|endOpenF: "{tag}"|]
        , textF    = \tag        -> putStrLn [qc|textF: "{tag}"|]
        , closeF   = \tag        -> putStrLn [qc|closeF: "{tag}"|]
        , cdataF   = \tag        -> putStrLn [qc|cdataF: "{tag}"|]
        }


main :: IO ()
main = do
    -- bs <- BS.readFile "data/books-1kb-utf16.xml"
    bs <- BS.readFile "data/books-1kb-utf16le.xml"
    case convertToZeroTerminated $ interpretAsUTF16 bs of
      LittleEndian bs' -> do
        putStrLn "Little Endian"
        printProcessing bs'
      BigEndian bs' -> do
        putStrLn "Big Endian"
        printProcessing bs'
